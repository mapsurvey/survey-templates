library(shiny)
library(shinyjs)
library(openlayers)
library(haffutils)
library(here)

## function to clear drawing
jsClear <- "shinyjs.clearFeatures = function() {
             olWidget.drawSource.clear();
           }"

ui <- fluidPage(
  useShinyjs(),
  extendShinyjs(text = jsClear),
  fillPage(h1("Hometown Location"),
           p("Click to place a point on your hometown (i.e. the town you
     were living in prior to college). If necessary, you can erase
     the point with the 'Clear point' button. When you are
     finished, click the 'Done' button."),
     ## have to fiddle with spacing to get it centered
     fluidRow(column(5),
              column(4,
                     actionButton("clear", "Delete point"),
                     actionButton("done", "Done  >>   ")),
              column(4)),
     br(),
     olOutput("map", height = "100vh"))
)

## TODO need to figure out how to delete first point if another one is clicked
server <- function(input, output) {
  onclick("clear", js$clearFeatures())

  output$map <- renderOl({
    ol(options = ol_options(debug = TRUE)) %>%
      add_stamen_tiles(layer = "toner") %>%
      set_view(-91.50, 43.00, 6) %>%
      add_draw("Point")
  })

  observeEvent(input$map_click, {
    print(input$map_click)
    hometown.loc <- input$map_click
  })

  ## when "done" button is clicked
  observe({
    if (input$done > 0) {
      lng <- input$map_click$lng
      lat <- input$map_click$lat
      df <- data.frame(lng = lng, lat = lat)
      ## TODO will need to ensure that this directory exists with proper
      ## permissions

      write.csv(df,
                file = here("data/hometown-map", rand_name("", ext = ".csv")),
                row.names = FALSE)
      ## TODO go to next page when button is clicked
    }
  })
}

shinyApp(ui, server)
