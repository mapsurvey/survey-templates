library(shiny)

## Only run examples in interactive R sessions
if (interactive()) {
  caption <- "Enter your hometown (i.e. the town you were living in prior to college) below as 'City, State'. Be sure to write out the full state name rather than just an abbreviation"

  ui <- fluidPage(
    textInput("caption", caption, "Ex: Eau Claire, Wisconsin"),
    p("When you are finished, click the 'Next' button")
  )
  server <- function(input, output) {
  }
  shinyApp(ui, server)
}
