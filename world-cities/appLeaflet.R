library(leaflet.extras)

leaflet() %>%
  addTiles() %>%
  addDrawToolbar(polylineOptions = FALSE,
                 polygonOptions = FALSE,
                 circleOptions = FALSE,
                 rectangleOptions = FALSE,
                 circleMarkerOptions = FALSE,
                 editOptions = editToolbarOptions(selectedPathOptions = selectedPathOptions()
))
